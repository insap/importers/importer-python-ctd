import os
import argparse

# |--------------------------------------------------------------------------
# | Basic application requirements
# |--------------------------------------------------------------------------
from app import importer
from app.Illuminate.Storage import Storage
from app.Illuminate.ParamStorage import ParamStorage

# |--------------------------------------------------------------------------
# | Consts
# |--------------------------------------------------------------------------
VERSION = '3.0'
GIT_URL = 'https://gitlab.com/insap/importers/importer-python-ctd'


# |--------------------------------------------------------------------------
# | Info
# |--------------------------------------------------------------------------
# | Общая информация по импортеру
# | Если у импортера имеются зависимости в виде библиотек не входящих в стандартный набор питона
# | То необходимо указать команды для их установки через pip в файле requirements.json
# | После установки зависимостей сервис проверит импортер, передав туда параметр --test=1
# | Импортер должен вернуть код 0, если все ОК
# |

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--test', type=bool, default=False)

    args = parser.parse_args()

    # При установки импортера в сервис он вызывается с параметром --test, приложению необходимо вернуть код 0
    # чтобы сервис понял, что все работает как надо(Например, подтянулись зависимости из файла requirements.json)
    if args.test:
        exit(0)

    # Путь к текущей директории
    absolute_path = os.path.dirname(os.path.abspath(__file__))

    # Директория с данными из сервиса
    data_path = os.path.join(absolute_path, "data")
    # Директория с файлами, которые пришли из сервиса
    files_path = os.path.join(data_path, "files")

    # Директории куда импортер будет складывать результат своей работы
    result_path = os.path.join(absolute_path, "result")
    result_files_path = os.path.join(result_path, "files")

        # Очищаем директории перед тем как их заполнять, и заново создаем их пустыми
    Storage.remove_all_files_from_path(result_path, re_create_path=True)
    Storage.remove_all_files_from_path(result_files_path, re_create_path=True)

    # Получаем данные которые пришли в импортер из сервиса
    # params - данные из формы, которую заполнял юзер для импорта
    params = Storage.read_data_from_json_file(os.path.join(data_path, "params.json"))
    # files - это файл с данными о расположении файлов, которые юзер загружал для импорта
    files = Storage.read_data_from_json_file(os.path.join(data_path, "files.json"))
    # data - данные предыдущего этапа обработки данных (сейчас пока один этап, поэтому это пустая переменная)
    data = Storage.read_data_from_json_file(os.path.join(data_path, "data.json"))

    # Класс для удобной работы с параметрами
    storage = ParamStorage(
        files_path,
        params,
        files,
        data
    )

    # Запускаем импортер
    result = importer.process(storage)

    # Записываем обработанные данные в файл result/data.json
    Storage.write_json_to_file(os.path.join(result_path, "data.json"), result)

    # Копируем params которые к нам пришли из сервиса и помещаем их в result/params.json
    Storage.write_json_to_file(os.path.join(result_path, "params.json"), storage.get_params())

    # Копируем files которые к нам пришел из сервиса и помещаем их в result/files.json
    Storage.write_json_to_file(os.path.join(result_path, "files.json"), storage.get_files())

    # files_path указывает на папку с файлами(data/files) перебираем в цикле все файлы
    # Копируем все файлы из data/files в result/files/
    for file_name in os.listdir(files_path):
        Storage.copy_file(os.path.join(files_path, file_name), os.path.join(result_files_path, file_name))

    exit(0)
