from openpyxl import load_workbook

from app.Illuminate.ParamStorage import ParamStorage


def process(storage: ParamStorage) -> list:
    wb = load_workbook(filename=storage.get_file_by_type('data'))  # Первичные данные
    sheet_ranges = wb['Data']

    row_count = sheet_ranges.max_row  # кол-во строк
    column_count = sheet_ranges.max_column  # кол-во столбцов

    list_station = []
    start_station = [3]
    end_station = []
    row = 3
    number_station = 1

    density_anomaly = 0
    i = 0
    for i in range(1, column_count + 1):
        cell = str(sheet_ranges.cell(row=2, column=i).value).lower()

        if cell == 'Density Anomaly'.lower():
            density_anomaly = i

    # Границы станций по стб Density Anomaly
    while i != row_count - 1:
        cell = sheet_ranges.cell(row=i, column=density_anomaly).value
        if cell is None or cell < 0:
            end_station.append(i)

            while True:
                cell = sheet_ranges.cell(row=i, column=density_anomaly).value
                if (cell is None or cell < 0) and i != row_count - 2:
                    i += 1
                break

            start_station.append(i)
        i += 1

    # Поиск максимальной глубины на каждой станции
    for i in range(0, len(start_station) - 1):
        stations = {'number_station': number_station, 'start_station': start_station[i], 'end_station': end_station[i]}
        depth = 0
        depth_index = 0
        for i in range(start_station[i], end_station[i]):
            if sheet_ranges.cell(row=i, column=8).value > depth:
                depth = sheet_ranges.cell(row=i, column=8).value
                depth_index = i
        max_depth_station = depth
        max_depth_index = depth_index
        # max_depth_station, max_depth_index = max_depth(start_station[i], end_station[i])
        if max_depth_station > 1:
            stations.update({'max_depth_st': max_depth_station,
                             'max_depth_ind': max_depth_index})  # Проверка на ложные станции, глубина > 1
            list_station.append(stations)
            number_station += 1

    workbook_station = load_workbook(filename=storage.get_file_by_type('coordinates'))  # Файл с координатами
    sheet_ranges_station = workbook_station['Лист1']
    row_count_station = sheet_ranges_station.max_row

    result = []
    row_numb = 1
    date_time = storage.get_params()['date_time']
    for i in range(0, len(list_station)):  # Добавление данных в result
        begin_station = list_station[i]['max_depth_ind']
        end_station = list_station[i]['end_station']
        station_ind = list_station[i]['number_station']
        for j in range(begin_station, end_station):
            if sheet_ranges.cell(row=j, column=8).value < 0.3:
                continue
            res = dict({
                'date': date_time,
                'temperature': sheet_ranges.cell(row=j, column=3).value,
                'turbidity': sheet_ranges.cell(row=j, column=5).value,
                'chlorophyll_a': sheet_ranges.cell(row=j, column=6).value,
                'depth': sheet_ranges.cell(row=j, column=8).value,
                'salinity': sheet_ranges.cell(row=j, column=9).value,
                'speed_of_sound': sheet_ranges.cell(row=j, column=12).value,
                'step_id': row_numb,
                'station': station_ind,
            })

            latitude = 0
            longitude = 0

            # Добавление координат
            for k in range(2, row_count_station + 1):
                if sheet_ranges_station.cell(row=k, column=1).value == station_ind:
                    latitude = sheet_ranges_station.cell(row=k, column=2).value
                    if latitude[2] == '°':
                        latitude = str(int(latitude[0:2]) + round((float(latitude[3:]) / 60), 5))
                    longitude = sheet_ranges_station.cell(row=k, column=3).value
                    if longitude[2] == '°':
                        longitude = str(int(longitude[0:2]) + round((float(longitude[3:]) / 60), 5))

                    ntu_1 = 0.0
                    ntu_2 = 0.0
                    ntu_average = 0.0

                    if sheet_ranges_station.cell(row=k, column=4).value:
                        ntu_1 = str(sheet_ranges_station.cell(row=k, column=4).value)

                    if sheet_ranges_station.cell(row=k, column=5).value:
                        ntu_2 = str(sheet_ranges_station.cell(row=k, column=5).value)

                    if sheet_ranges_station.cell(row=k, column=6).value:
                        ntu_average = str(sheet_ranges_station.cell(row=k, column=6).value)

                    break

            res['depth2'] = -1 * sheet_ranges.cell(row=j, column=8).value
            res['latitude'] = latitude
            res['longitude'] = longitude
            res['ntu_1'] = ntu_1
            res['ntu_2'] = ntu_2
            res['ntu_average'] = ntu_average

            result.append(res)
            row_numb += 1
    wb.close()
    workbook_station.close()

    return result
